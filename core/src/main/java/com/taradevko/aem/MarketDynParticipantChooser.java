package com.taradevko.aem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Component;

import com.adobe.granite.workflow.payload.PayloadInfo;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.ParticipantStepChooser;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.metadata.MetaDataMap;

@Component(immediate = true, property = {"chooser.label=Market approver chooser"})
public class MarketDynParticipantChooser implements ParticipantStepChooser {

    private static final String USER_ADMIN = "admin";
    private static final String GROUP_MARKET_APPROVER = "market-approvers-";
    private final Pattern pattern = Pattern.compile("/content/we-retail/(\\w{2})/.*");

    @Override
    public String getParticipant(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap)
            throws WorkflowException {

        WorkflowData workflowData = workItem.getWorkflowData();
        if (PayloadInfo.PAYLOAD_TYPE.JCR_PATH.toString().equals(workflowData.getPayloadType())
                && workflowData.getPayload() != null) {

            String payloadPath = (String) workflowData.getPayload();
            Matcher matcher = pattern.matcher(payloadPath);
            if (matcher.find()) {
                String market = matcher.group(1);
                return GROUP_MARKET_APPROVER + market;
            }
        }
        return USER_ADMIN;
    }
}
